    // booking
    var form1 = document.getElementById("form1");

    form1.addEventListener('submit',function(event){
        event.preventDefault();
    
        var place = document.getElementById("place").value;
        // console.log("Place :",place);
    
        var people = document.getElementById("count").value;
        // console.log("How Many People: ",people);
    
        var firstDate = document.getElementById("date1").value;
        // console.log("Starting Date: ",firstDate);
    
        var lastDate = document.getElementById("date2").value;
        // console.log("Arrival Date: ",lastDate);


        let user_records = new Array();
        user_records = JSON.parse(localStorage.getItem("users")) ? JSON.parse(localStorage.getItem("users")):[];

        user_records.push({
            "Place": place,
            "People": people,
            "First Date": firstDate,
            "Last Date": lastDate
        })    
        localStorage.setItem("users",JSON.stringify(user_records));
        alert("THANK YOU FOR BOOKING. HOPE YOU ENJOY OUT TRIPS.");
        window.location.href = "web.html";
    
    })