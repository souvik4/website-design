    // when click show forgot password form

$('.Otp a').click(function(){
    $('#form').hide();
    $('#Forgotform').show();
})


$('#Forgotlog').click(function(){
    $('#Forgotform').hide();
    $('#form').show();
})


        // eye show password

        const eye3 = document.querySelector('#eye3');
        const fOtp = document.querySelector('#fOtp');
       
        eye3.addEventListener('click', function (e) {
          // toggle the type attribute
          const type = fOtp.getAttribute('type') === 'password' ? 'text' : 'password';
          fOtp.setAttribute('type', type);
          // toggle the eye slash icon
          this.classList.toggle('fa-eye-slash');
      });



        const eye4 = document.querySelector('#eye4');
        const fPassword = document.querySelector('#fPassword');
       
        eye4.addEventListener('click', function (e) {
          // toggle the type attribute
          const type = fPassword.getAttribute('type') === 'password' ? 'text' : 'password';
          fPassword.setAttribute('type', type);
          // toggle the eye slash icon
          this.classList.toggle('fa-eye-slash');
      });




    //   ForgotPassword validation

    var Femail = document.forms['Forgotform']['fEmail'];
    var Fotp = document.forms['Forgotform']['fOtp'];
    var Fpassword = document.forms['Forgotform']['fPassword'];

    var forgotPass_error = document.getElementById("forgotPass_error");

    Femail.addEventListener('textInput',Femail_verify);
    Fotp.addEventListener('textInput',Fotp_verify);
    Fpassword.addEventListener('textInput',Fpass_verify);


    function ForgotPasValid(){
        let fresult = []
    
    if(Fpassword.value.length < 9) {
        Fpassword.style.border = "2px solid red";
        forgotPass_error.style.display = "block";
        Fpassword.focus();
        fresult.push("you must have 9 char");
    }
   
    if (Fpassword.value.search(/[a-z]/i) < 0){
        Fpassword.style.border = "2px solid red"; 
        forgotPass_error.style.display = "block";
        Fpassword.focus();
        fresult.push("you must have one alphabet")
    } 

    if (fresult.length > 0) {
        document.getElementById("forgotPass_error").innerHTML =fresult.join(",");
        return false;
    }

    if (Femail === "" && Fpassword === "" && Fotp === ""){
        console.log("filled is empty");
     }
     else{
         console.log("FORGOT PASSWORD SUCCESFULLY");
        //  window.location.href = "web.html";
     }   
    }


    function Femail_verify(){
        if(Femail.value.length >= 8) {
            Femail.style.border = "2px solid green";
            return true;
        }
    }
    
    
    function Fotp_verify(){
        if(Fotp.value.length >= 3) {
            Fotp.style.border = "2px solid green";
            return true;
        }
    }

    function Fpass_verify(){
        if(Fpassword.value.length >= 8 ) {
            Fpassword.style.border = "2px solid green";
            forgotPass_error.style.display = "none";
            return true;
        }
    }



    // forgot password form show
var Forgotform = document.getElementById("Forgotform");

Forgotform.addEventListener('submit',function(event){
    event.preventDefault();

    var for_Email = document.getElementById("fEmail").value;
    console.log(for_Email);    

    var for_Otp = document.getElementById("fOtp").value;
    console.log(for_Otp); 

    var for_Password = document.getElementById("fPassword").value;
    console.log(for_Password);    

    let user_records = new Array();
    user_records = JSON.parse(localStorage.getItem("users")) ? JSON.parse(localStorage.getItem("users")):[];
    if(user_records.some((v) => {return v.Email == for_Email}))  
    {
        if(for_Password.length >= 8 && for_Password.search(/[a-z]/i) > 0 && for_Otp > 3)
        {

            // user_records = user_records.filter(el => el.Email!== for_Email) or map 
            user_records = user_records.map((el) => {
                   if(el.Email === for_Email){
                    el.Otp = for_Otp 
                    el.Password = for_Password;
                   }
                   return el;
                                      
            })

            localStorage.setItem("users",JSON.stringify(user_records));
            alert("PASSWORD FORGOT SUCCESSFULLY. NOW IT'S TIME TO LOGIN! ");
            console.log(user_records);
             window.location.href = "web.html";


            
            
        
        } 
        else{
            alert("You write wrong format in password");
        }
    }
    else{
        alert("YOU NEED TO SIGN UP FIRST!");
        window.location.href = "web.html";
    }
      
})
